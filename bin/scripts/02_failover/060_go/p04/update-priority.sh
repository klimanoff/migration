#!/usr/bin/env bash

set -euo pipefail
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
UNSYMLINKED_SCRIPT_DIR="$(readlink -f "${SCRIPT_DIR}" || readlink "${SCRIPT_DIR}" || echo "${SCRIPT_DIR}")"
# shellcheck disable=SC1091,SC1090
source "${UNSYMLINKED_SCRIPT_DIR}/../../../../workflow-script-commons.sh"

if [[ -z $POSTGRESQL_AZURE_PRIMARY ]]; then
  echo "You must set POSTGRESQL_AZURE_PRIMARY in source_vars"
fi
ssh_host "$POSTGRESQL_AZURE_PRIMARY"  "sudo gitlab-psql -t -d gitlab_repmgr -c \"update repmgr_gitlab_cluster.repl_nodes set priority=100 where name like '%g${FAILOVER_ENVIRONMENT}%'\""
